#!/bin/sh

# Copyright (c) 2018, 2020, 2024 Mikhail Zakharov <zmey20000@yahoo.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


# bfmerge.sh - Compare and merge two regular or device files block by block

# With intentions to follow oldschool Bourne Shell syntax
# Should run on FreeBSD, MacOS and Linux

# 2018.07.26	v0.1	Mikhail Zakharov <zmey20000@yahoo.com>
#			Initial release. Linux-only version
# 2018.07.27	v0.2	Mikhail Zakharov <zmey20000@yahoo.com>
#			Chech for root access; FreeBSD and MacOS support added
# 2020.07.24	v0.5	Mikhail Zakharov <zmey20000@yahoo.com>
#			Multiple bugs fixed; Color output
# 2024.01.01	v0.6	Mikhail Zakharov <zmey20000@yahoo.com>
#			* Many bugs fixed
#			* Better compatibility
#			* Verbosity level 2 now has colors
#			* Verbosity level 3 is now fast and colorless


# -- DEFAULTS ------------------------------------------------------------------
bs=512				# System blocksize for block devices
src=""				# Source file [SRC]
dst=""				# Destination file [DST]
batch=0				# Silently apply differences from [SRC] to [DST]
fullscan=1			# Search for all differences - 1
			    	# or quit on size mismatch - 0
verbosity=1			# Verbosity level: 0-3. Default: 1
DEBUG=0				# Set 1 to turn debug on
color=31			# Red color by default
					# Color values
					#	0	Not set
					#	31	Red
					#	32	Green
					#	33	Yellow
					#	34	Purple
					#	35	Magenta
					#	36	Cyan

# -- FUNCTIONS -----------------------------------------------------------------
usage() {
	cat <<EOF
bfmerge.sh - Compare and merge two regular or device files block by block

Usage:
	bfmerge.sh -s src.bin -d dst.bin [-b size][-v 0-3][-BfD] [-c color][-h]

Where:
	-s	Source file to compare [SRC]
	-d	Destination file [DST]
	-b	Block size. Default is 512 bytes
	-v	Verbosity level 0 to 2. Default is 1
	-f	Enable fullscan mode. Do not stop if [SRC] and [DST] filesizes
		differ
	-D	Enable debug mode
	-c	Pick red|green|yellow|purple|magenta|cyan color to highlight
		diffs for verbosity level 2; Default color - red
	-h	This help
EOF
	exit 2
}

quit() {
	# Print error message to stderr and exit the script with an error code

	# $1 Message to print
	# $2 errorcode to exit with

	[ "$1" ] && printf "FATAL: $1\n" >&2
	[ "$2" ] && exit $2
	exit 2
}

debugprint() {
	# if DEBUG > 1 print formatted message
	[ "$DEBUG" -gt 0 ] && printf "DEBUG: $*\n" >&2
}

equalprint() {
	# State files ar qual and exit

	# $1 must be ==, > or <

	[ "$verbosity" -gt 0 ] && printf "Filesize [$src] $1 [$dst]\n"
	[ "$verbosity" -gt 1 ] && {
		printf "SRC type: $src_type\tname: $src\tbytes/blocks: $src_size/$src_blocks\n" 1>&2;
		printf "DST type: $dst_type\tname: $dst\tbytes/blocks: $dst_size/$dst_blocks\n" 1>&2;
	}
}

checkfile() {
	# Check if the $2 is a regular file or a device

	[ "$#" -ne 2 ] && quit "checkfile() requires 2 arguments"

	[ ! -e "$2" ] && quit "[$2] File does not exist!"
	[ -f "$2" ] && eval $1="file" && return 0			# File
	[ -b "$2" -o -c "$2" ] && eval $1="dev" && return 1		# Device

	# Not a valid file type
	quit "[$2] is not a regular file or a device!"
}

getsize() {
	# Get size of a file or a device

	# $1 return variable: size in bytes
	# $2 file name
	# $3 type: 0 - file, 1 - device

	[ "$#" -ne 3 ] && quit "getsize() requires 3 arguments"
	[ "$3" != "file" -a  "$3" != "dev" ] &&
		quit "[$2] checkfile [$3] is unsupported!"

	[ $3 = "dev" ] && bdev=`basename "$2"`

	case "$OS" in
		Darwin)
			[ -z "$md5" ] && alias md5="md5"
			[ "$3" = "file" ] && size=`stat -f "%z" "$2"`
			[ "$3" = "dev" ] && size=`diskutil info "$2" |
				awk 'BEGIN {FS = " +|[(]"} /Size/ {print $7}'`
			;;
		FreeBSD)
			[ -z "$md5" ] && alias md5="md5"
			[ "$3" = "file" ] && size=`stat -f "%z" "$2"`
			[ "$3" = "dev" ] &&
				size=`sysctl -n kern.geom.conftxt |
				awk -v dev="$bdev" '$3 == dev {print ($4)}'`
			;;
		Linux)
			[ -z "$md5" ] && alias md5='md5sum -b | cut -f 1 -d " "'
			[ "$3" = "file" ] && size=`stat -c "%s" "$2"`
			[ "$3" = "dev" ] &&
				size=`(printf "$bs * ";
					cat /sys/class/block/$bdev/size) | bc`
			;;
		*)
			quit "Do not know how to run on $OS"
			;;
	esac

	eval $1='$size'
}

set_dev() {
	# $1 - associated device
	# $2 - file to set as device

	case "$OS" in
		Darwin)
			dsk=`hdiutil attach -imagekey diskimage-class=CRawDiskImage -nomount $2 | tr -d '[:space:]'`
			;;
		FreeBSD)
			dsk=/dev/`mdconfig -f $2`
			;;
		Linux)
			dsk=`losetup -f  --show $2`
			;;
		*)
			quit "Do not know how to set loop device on $OS"
			;;
	esac

	eval $1='$dsk'

}

unset_dev() {
	# $1 - associated loop device

	case "$OS" in
		Darwin)
			hdiutil detach $1 >/dev/null
			;;
		FreeBSD)
			mdconfig -d -u $1
			;;
		Linux)
			losetup -d $1
			;;
		*)
			quit "Do not know how to set loop device on $OS"
			;;
	esac
}

size2blocks() {
	# Convert size in bytes to blocks rounded to the upper value

	# $1 return value: number of blocks
	# $2 size in bytes
	# $3 blocksize

	[ "$#" -ne 3 ] && quit "size2blocks() requires 3 arguments"

	[ `printf "$2 %% $3\n" | bc` -gt 0 ] &&
		eval $1=`printf "$2 / $3 + 1\n" | bc` ||	# Remainder > 0
		eval $1=`printf "$2 / $3\n" | bc`		# Remainder = 0
}

cmpfiles() {
	# Compare files and show their diffs

	[ "$src" = "$dst" ] && { equalprint "=="; exit 0; } || {
		[ $src_blocks -lt $dst_blocks -o $src_size -lt $dst_size ] && {
			equalprint "<";

			debugprint "fullscan $fullscan";
			# fullscan == 0 then exit the script immediately else continue
			[ $fullscan -eq 0 ] && quit "" 1
		}

		[ $src_blocks -gt $dst_blocks -o $src_size -gt $dst_size ] && {
			equalprint ">";
			quit "[SRC] size must not be greater then [DST] size" 1;
		}
	}

	[ $src_blocks -lt $dst_blocks ] &&
		lim_blocks=$src_blocks || lim_blocks=$dst_blocks

	# Count the number of lines in xxd output for the block size $bs.
	# 16 is the number of bytes per xxd line
	oxxdl=`expr $bs "/" 16`

	# Decrease the value by one as we count offsets from zero block
	lim_blocks=`expr $lim_blocks - 1`
	for offset in `seq 0 $lim_blocks`; do
		debugprint "Block #: $offset/$lim_blocks"

		src_block_sum=`dd if="$src" bs="$bs" skip="$offset" count=1 2>/dev/null | md5`
		dst_block_sum=`dd if="$dst" bs="$bs" skip="$offset" count=1 2>/dev/null | md5`

		debugprint "MD5 SRC/DST: $src_block_sum/$dst_block_sum"

		[ $src_block_sum != $dst_block_sum ] && {
			xs=`dd if="$src" bs="$bs" skip="$offset" count=1 2>/dev/null | xxd | cut -c 1-50`
			[ "$xs" = "" ] && quit "Unable to read [$src]"
			xd=`dd if="$dst" bs="$bs" skip="$offset" count=1 2>/dev/null | xxd | cut -c 1-50`
			[ "$xd" = "" ] && quit "Unable to read [$dst]"

			# Number of lines in the current block
			xsl=`echo "$xs" | wc -l`
			xdl=`echo "$xd" | wc -l`

			case "$verbosity" in
				0)	# Zero verbosity
					printf "Files [$src] and [$dst] differ\n"
					exit 1
					;;

				1|2)	# Default verbosity level 1: better precision; Level 2 - full blocks with color diff
					for l in `seq 1 $oxxdl`; do
						[ $l -le $xsl ] &&
							sl=`echo "$xs" | cut -c 11-49 | awk -v l="$l" 'NR == l { print }'` ||
							sl=""
						[ $l -le $xdl ] &&
							dl=`echo "$xd" | cut -c 11-49 | awk -v l="$l" 'NR == l { print }'` ||
							dl=""

						if [ "$sl" != "" -a "$dl" = "" ]; then
							# Properly align source when printing if there's no destination
							printf "%8.8x+%2.2x: %s |\n" "$offset" "$l" "$sl"
						fi

						[ $color -eq 0 ]  && {
							# No color
							echo "$sl" | grep -q -v "$dl" && {
								if [ "$dl" != "" -a "$sl" = "" ]; then
									# Properly align dest when printing if there's no src
									printf "%8.8x+%2.2x:                                         | %s\n" "$offset" "$l" "$dl"
								else
									printf "%8.8x+%2.2x: %b | %b\n" "$offset" "$l" "$sl" "$dl"
								fi;
							} || {
								# Print identical data chunks
								[ $verbosity -eq 2 ] && printf "%8.8x+%2.2x: %b | %b\n" "$offset" "$l" "$sl" "$dl"
							}
						} || {
							# Slow color output
							echo "$sl" | grep -q -v "$dl" && {
								if [ "$dl" != "" -a "$sl" = "" ]; then
									# Properly align dest when printing if there's no src
									printf "%8.8x+%2.2x:                                         | %b\n" "$offset" "$l" "\033[0;${color}m""$dl""\033[0m"
								else
									ndl=''
									for n in `seq 1 8`; do
										sn=''; dn=''

										_sn=0
										for sn in $sl; do
											_sn=`expr $_sn + 1`
											[ $_sn -ge $n ] && break
										done

										_dn=0
										for dn in $dl; do
											_dn=`expr $_dn + 1`
											[ $_dn -ge $n ] && break
										done

										[ $_sn -le $_dn ] && {
											[ "$sn" != "$dn" ] &&
												ndl="$ndl""\033[0;${color}m""$dn""\033[0m " ||
												ndl="$ndl""$dn ";
										}
									done
									printf "%8.8x+%2.2x: %s | %b\n" "$offset" "$l" "$sl" "$ndl"
								fi;
							} || {
								# Print identical data chunks
								[ $verbosity -eq 2 ] && printf "%8.8x+%2.2x: %b | %b\n" "$offset" "$l" "$sl" "$dl"
							}
						}
					done
					;;

				3)	# Maximum verbosity level 3: Print full blocks; no colors
					printf "\nSRC | DST block: %#x (%s) MD5: $src_block_sum | $dst_block_sum\n" "$offset" "$offset"
					for l in $(seq 1 $oxxdl); do
						[ $l -le $xsl ] &&
							echo "$xs" | awk -v l="$l" 'BEGIN {ORS=" "} NR==l {$0=substr($0, 5, length - 4); print}' ||
							printf "                                               "

						[ $l -le $xdl ] &&
							echo "$xd" | awk -v l="$l" -F ":" 'NR==l {$0=substr($0, 11, length - 9); print ( "|", $0)}' ||
							printf "\n"
					done
					;;
				*)
					quit "Wrong verbosity level: $verbosity"
					;;
			esac

			printf "Apply this block from SRC to DST [Y/n]: "; read apply
			[ "$apply" = "Y" ] && {
				dd if="$src" of="$dst" bs="$bs" skip="$offset" seek="$offset" count=1 2>/dev/null
				[ $? -ne 0 ] && quit "Failed to fix $dst block: $offset" 2
				apply="N"
				debugprint "Block: $offset fixed"
			}
		}
	done
}

# -- MAIN ----------------------------------------------------------------------
trap "quit 'Exit on signal' 1" SIGINT SIGTERM

[ `id -u` -ne 0 ] && quit "You must be root to run bfmerge.sh" 2

args=`getopt s:d:b:v:fDc:h $*`
[ $? -ne 0 ] && quit "getopt(): failed to parse options" 2

eval set -- $args
for arg; do
	case "$arg" in
		-s)
			src="$2"; shift; shift ;;
		-d)
			dst="$2"; shift; shift ;;
		-b)
			bs="$2"; shift; shift ;;
		-v)
			verbosity="$2"; shift; shift ;;
		-f)
			fullscan=1; shift ;;
		-D)
			DEBUG=1; shift ;;
		-h)
			usage ;;
		-c)
			case "$2" in
				[rR][eE][dD])			color=31 ;;
				[gG][rR][eE][eE][nN])		color=32 ;;
				[yY][eE][lL][lL][oO][wW])	color=33 ;;
				[pP][uU][rR][pP][lL][eE])	color=34 ;;
				[mM][aA][gG][eE][nN][tT][aA])	color=35 ;;
				[cC][yY][aA][nN]) 		color=36 ;;
				*)		 		color=0  ;;
			esac
			shift; shift ;;
		--)
			shift; break ;;
	esac
done

debugprint "Command-line args: $args"
debugprint "Options: src: [$src] dst: [$dst] bs: [$bs] verbosity: [$verbosity] \
fullscan: [$fullscan] DEBUG: [$DEBUG] color: [$color]"

[ "$src" = "" -o "$dst" = "" ] && usage

checkfile src_type "$src"		# Type: 0 - file, 1 - device
checkfile dst_type "$dst"

OS=`uname -s`
debugprint "OS: $OS"

getsize src_size "$src" "$src_type"
getsize dst_size "$dst" "$dst_type"

[ "$src_type" = "file" ] && {
	src_file="$src"; set_dev src "$src"
	[ "$verbosity" -gt 1 ] && printf "[$src_file] opened as [$src] device\n"
	debugprint "[$src_file] opened as [$src] device"

	printf "WARNING: Accessible size of [%s] is rounded by block boundary!\n" "$src_file" >&2
}
[ "$dst_type" = "file" ] && {
	dst_file="$dst"; set_dev dst "$dst"
	[ "$verbosity" -gt 1 ] && printf "[$dst_file] opened as [$dst] device\n"
	debugprint "[$dst_file] opened as [$dst] device"

	printf "WARNING: Accessible size of [%s] is rounded by block boundary!\n" "$dst_file" >&2
}

debugprint "MD5 command: $md5"

size2blocks src_blocks "$src_size" "$bs"
size2blocks dst_blocks "$dst_size" "$bs"

debugprint "SRC type: $src_type\tname: $src\tbytes/blocks: $src_size/$src_blocks"
debugprint "DST type: $dst_type\tname: $dst\tbytes/blocks: $dst_size/$dst_blocks"

cmpfiles
[ -n "$src_file" ] && unset_dev "$src"
[ -n "$dst_file" ] && unset_dev "$dst"
