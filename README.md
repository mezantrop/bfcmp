# bfcmp - Compare and merge two regular or device files block by block

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

With intentions to follow oldschool Bourne Shell syntax. Should run on FreeBSD,
MacOS and Linux

## bfcmp.sh - Compare two regular or device files block by block

![bfcmp](bfcmp.png)

```
bfcmp.sh - Compare two regular or device files block by block

Usage:
        bfcmp.sh -s src.bin -d dst.bin [-b size][-v 0-4][-f][-D] [-c color][-h]

Where:
        -s      Source file to compare [SRC]
        -d      Destination file [DST]
        -b      Block size. Default is 512 bytes
        -v      Verbosity level 0 to 4. Default is 2
        -f      Enable fullscan mode. Continue to scan for different blocks even
                if [SRC] and [DST] filesizes are different
        -D      Enable debug mode
        -c      Pick red|green|yellow|purple|magenta|cyan color to highlight
                diffs for verbosity levels 2 and 3. Default color - red
        -h      This help

Verbosity levels:
        0       Almost zero verbosity
        1       Print different block numbers only
        2       Better precision withing blocks
        3       Full slow block outup with colors
        4       Full fast block outup no colors

```

## bfmerge.sh - Compare and merge two regular or device files block by block

```
bfmerge.sh - Compare and merge two regular or device files block by block

Usage:
        bfmerge.sh -s src.bin -d dst.bin [-b size][-v 0-3][-BfD] [-c color]

Where:
        -s      Source file to compare [SRC]
        -d      Destination file [DST]
        -b      Block size. Default is 512 bytes
        -v      Verbosity level 0 to 2. Default is 1
        -f      Enable fullscan mode. Do not stop if [SRC] and [DST] filesizes
                differ
        -D      Enable debug mode
        -c      Pick red|green|yellow|purple|magenta|cyan color to highlight
                diffs for verbosity level 2; Default color - red
        -h      This help
```
